using UnrealBuildTool;

public class midtermTarget : TargetRules
{
	public midtermTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("midterm");
	}
}
